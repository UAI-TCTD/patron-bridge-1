﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD.Patrones.Bridge
{
    public class Cuadrado : Figura
    {
        public Cuadrado(IColor color) : base(color) { }

        public override void AplicarColor()
        {
            Console.WriteLine("Dibujando un cuadrado");
            base.AplicarColor();

        }
    }
}
