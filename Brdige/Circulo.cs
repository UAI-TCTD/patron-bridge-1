﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD.Patrones.Bridge
{
   public class Circulo : Figura
    {
        public Circulo(IColor color) : base(color) { }

        public override void AplicarColor() {
            Console.WriteLine("Dibujando un circulo");
            base.AplicarColor();
        }
    }
}
