﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD.Patrones.Bridge
{
    class Rojo : IColor
    {
        public void AplicarColor()
        {
            Console.WriteLine("Aplicando color Rojo");
         }
    }
}
