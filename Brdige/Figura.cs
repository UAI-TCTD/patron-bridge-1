﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD.Patrones.Bridge
{
    public abstract  class Figura
    {
        protected IColor color; 

        public Figura(IColor color) //agrego por constructor
        {
            this.color = color;
        }
        public virtual void AplicarColor()
        {
            color.AplicarColor();
        }
        
    }
}
