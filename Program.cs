﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD.Patrones.Bridge
{
    class Program
    {
        static void Main(string[] args)
        {
            IColor color = new Rojo();
            Figura figura = new Cuadrado(color);
            figura.AplicarColor();
            Console.ReadKey();
        }
    }
}
